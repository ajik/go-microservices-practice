# User Service

This is the User service

Generated with

```
micro new user
```

## Usage

Generate the proto code

```
make proto
```

Run the service

```
micro run .
```

Make sure to change package for client dand server in ****.pb.micro.go
by using these imports
```
	client "github.com/micro/micro/v3/service/client"
	server "github.com/micro/micro/v3/service/server"
```
somehow, it always generate v2 micro/i do not understand very well but it uses microhq, or something like that. Then also please remove in NewUserService method/function 
```
    if c == nil {
		c = client.NewClient()
    }
```

Call by terminal 
```
micro call user User.Call '{"name": "call 1"}'
```

CallV2 by terminal
```
micro call user User.CallV2 '{"name": "call v2"}'
```